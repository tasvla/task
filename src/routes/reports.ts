import express from "express";

import * as errorHandler from "../lib/async-response-handler";
import { ReportManager } from "../models/reports-manager";

const app = express();


/**
 * Get a report
 */
app.get(
    "/:pdf?",
    errorHandler.wrap(req => {
        const userId = 1827;
        const { pdf } = req.params;
        return new ReportManager().get(userId, pdf);
    })
);

export const route = app;
