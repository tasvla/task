import express from "express";

import * as errorHandler from "../lib/async-response-handler";
import { PostcodeManager } from "../models/postcodes-manager";

const app = express();


/**
 * Get postcodes
 */
app.get(
    "/",
    errorHandler.wrap(req => {
        return new PostcodeManager().getAll();
    })
);

export const route = app;
