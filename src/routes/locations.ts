import express from "express";

import * as errorHandler from "../lib/async-response-handler";
import { LocationManager } from "../models/locations-manager";
const app = express();


/**
 * Get a location
 */
app.get(
    "/:postcode/:filter",
    errorHandler.wrap(req => {
        const { postcode, filter } = req.params;
        return new LocationManager().get(postcode, filter);
    })
);

export const route = app;
