process.env.NODE_ENV = "test";
process.env.PORT = "8000";
process.env.databaseType = "sqlite";
process.env.databaseHost = "localhost";
process.env.databasePort = "3306";
process.env.databaseName = ":memory:";
process.env.databaseUsername = "dbuser";
process.env.databasePassword = "dbpass";

import { getManager } from "typeorm";

// tslint:disable
import { startServer } from "../";

before(async () => {
    console.info("BEFORE");
    await startServer();

    await getManager().query(`
    INSERT INTO postcodes (id, postcode, latitude, longitude)
    VALUES
        (1,'ZE2 9LJ',60.1962,-1.28337),
        (2,'ZE1 0JH',60.1497,-1.1367),
        (3,'ZE1 0LY',60.1451,-1.17136),
        (4,'ZE1 0QB',60.1528,-1.15245),
        (5,'ZE2 9NB',60.2837,-1.38381),
        (6,'ZE2 9NL',60.1795,-1.46612)`
    )

    await getManager().query(`
    INSERT INTO users (id, name, surname, email)
    VALUES
        (1,'Dee','UCYNW','IYWMV@SDLV.com'),
        (2,'J','SCFUK','XCQBH@JAVH.com'),
        (3,'Karen','REVRV','VLSIJ@XMTG.com'),
        (4,'S','GSUAO','COSVC@VAOU.com'),
        (5,'Jean','UKPTD','IHLKP@VMWB.com'),
        (6,'Darryl','HEWWV','OQMQT@VYGL.com'),
        (7,'Terence','QQKAT','LAQGG@PITV.com'),
        (8,'Najma','XHPIT','AKAXQ@JAXP.com')`
    )

    await getManager().query(`
    INSERT INTO 'addresses'('id', 'postcode_id', 'district', 'locality', 'street', 'site', 'site_number', 'site_description', 'site_subdescription')
    VALUES
        (1, 1, '', '', 'Cairns Close', '', ' ', 'Anderson House', 'Flat 16'),
        (2, 2, '', '', 'Fulmore Close', '', '4', '', ''),
        (3, 3, '', '', 'Hedgebrooms', '', '9', '', '')`
    )

    await getManager().query(`
    INSERT INTO schools(id, name, postcode_id)
    VALUES
        (1, 'Haverstock School', 1),
        (2, 'Parliament Hill School', 2),
        (3, 'Regent High School', 3)`
    )

    await getManager().query(`
    INSERT INTO houses(id, user_id, postcode_id, address_id, propertytype, updated)
    VALUES
        (2610, 1, 1, 1, 1, '2016-07-05'),
        (2611, 2, 2, 2, 3, '2016-07-05'),
        (2639, 3, 3, 3, 1, '2016-07-05'),
        (2647, 4, 1, 1, 0, '2016-07-05'),
        (2653, 5, 2, 2, 1, '2016-07-05'),
        (2654, 6, 3, 3, 1, '2016-07-05'),
        (2655, 7, 1, 1, 0, '2016-07-05'),
        (2656, 8, 2, 2, 1, '2016-07-05')`
    )

    await getManager().query(`
    INSERT INTO busstops (id, name, lat, lon)
    VALUES
        (1,'Abbey',53.65537001,-0.31494875),
        (2,'Aln Court - Wansbeck House',54.97874010,-1.71788533),
        (3,'Ansley Road',52.51749061,-1.52545105),
        (4,'Armstrong Road - Atkinson Road',54.96912614,-1.66209290),
        (5,'Ash Drive',52.36360808,-2.05410246)`
    )
});

after(() => {
    console.info("AFTER");
    process.exit(0);
});
