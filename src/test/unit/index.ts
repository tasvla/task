import { assert } from "chai";
import { LocationManager } from "../../models/locations-manager";
import { PostcodeManager } from "../../models/postcodes-manager";
import { ReportManager } from "../../models/reports-manager";

// tslint:disable
describe("Unit tests", () => {

    let locationManager: LocationManager;
    let reportManager: ReportManager;
    let postcodeManager: PostcodeManager;

    const userId: number = 1;
    const postcode: string = "ZE2 9LJ";

    before(() => {
        locationManager = new LocationManager();
        postcodeManager = new PostcodeManager();
        reportManager = new ReportManager();
    })

    it("Should be able get postcodes", async () => {
        const getAllPostcodes = await postcodeManager.getAll();
        assert.isNotEmpty(getAllPostcodes);
    });

    it("Should be able get user report", async () => {
        const getUserReport = await reportManager.get(userId);
        assert.exists(getUserReport.userId);
        assert.exists(getUserReport.address);
    });

    it("Should be able get user related chats data", async () => {
        const getChatsData = await reportManager.geChatsData(userId);
        assert.exists(getChatsData.numberOfDiffChats);
    });

    it("Should be able get user related house data", async () => {
        const getHouseData = await reportManager.getHouseData(userId);
        assert.exists(getHouseData.fullName);
        assert.exists(getHouseData.address);
    });

    it("Should be able get user related likes data", async () => {
        const getLikesData = await reportManager.getLikesData(userId);
        assert.exists(getLikesData);
    });

    it("Should be able get user related people data", async () => {
        const getPeopleData = await reportManager.getPeoplesData(userId);
        assert.exists(getPeopleData);
    });

    it("Should be able to get address locations with filter", async () => {
        const getAddresses = await locationManager.get(postcode, "address");
        assert.isNotEmpty(getAddresses);
    });

    it("Should be able to get postcode addresses locations", async () => {
        const getPostcodeAddresses = await locationManager.postcodeAddresses(postcode);
        assert.isNotEmpty(getPostcodeAddresses);
        assert.exists(getPostcodeAddresses.addresses);
    });

    it("Should return empty array of addresses with non-existing postcode", async () => {
        const getPostcodeAddresses = await locationManager.postcodeAddresses("0");
        assert.isEmpty(getPostcodeAddresses.addresses);
    });
});
