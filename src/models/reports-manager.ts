import HTTP from "http-status-codes";
import { createPDF, jsonObjectToHTML } from "node-pdf-report";
import { getConnection, Repository } from "typeorm";
import { Chats, Likes, People, Users } from "../entities";
import { codedError } from "../lib/coded-error";
import { GetChatsDataResponse, GetHomeDataResponse, GetLikesDataResponse, GetPeopleDataResponse } from "../types/schema-generated";


export class ReportManager {

    private readonly usersTable: Repository<Users>;
    private readonly likesTable: Repository<Likes>;
    private readonly peopleTable: Repository<People>;
    private readonly chatsTable: Repository<Chats>;

    constructor() {
        this.usersTable = getConnection().getRepository(Users);
        this.likesTable = getConnection().getRepository(Likes);
        this.peopleTable = getConnection().getRepository(People);
        this.chatsTable = getConnection().getRepository(Chats);
    }

    /**
     * Returns report for specific user
     *
     * @param {number} userId
     * @param {string} pdf
     * @returns {Promise<any>}
     * @memberof ReportManager
     */
    async get(userId: number, pdf?: string):
        Promise<GetHomeDataResponse &
            GetLikesDataResponse &
            GetPeopleDataResponse &
            GetChatsDataResponse
        > {

        const [userLikes, userHome, userPeoples, userChats] = await Promise.all([
            this.getLikesData(userId),
            this.getHouseData(userId),
            this.getPeoplesData(userId),
            this.geChatsData(userId)
        ]);

        const data = { ...userHome, ...userLikes, ...userPeoples, ...userChats };
        if (pdf) {
            this.generatePdf(data);
        }

        return data;
    }

    /**
     * Returns house related data for specific user
     *
     * @param {number} userId
     * @returns {Promise<GetHomeDataResponse>}
     * @memberof ReportManager
     */
    async getHouseData(userId: number): Promise<GetHomeDataResponse> {
        try {
            const userHouse = await this.usersTable
                .createQueryBuilder("users")
                .where("users.id = :userId", { userId })
                .leftJoinAndSelect("users.house", "houses")
                .leftJoinAndSelect("houses.user", "peoples")
                .leftJoinAndSelect("houses.address", "addresses")
                .leftJoinAndSelect("addresses.postcode", "postcode")
                .getOne();

            const objectToReturn = {
                userId: userHouse!.id,
                fullName: userHouse!.name + " " + userHouse!.surname,
                houseId: userHouse!.house.id,
                propertyType: this.mapPropertyType(userHouse!.house.propertytype),
                address: { ...userHouse!.house.address, postcode: userHouse!.house.address.postcode.postcode }
            };
            return objectToReturn;
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Returns people that lives in user house
     *
     * @param {number} userId
     * @returns
     * @memberof ReportManager
     */
    async getPeoplesData(userId: number): Promise<GetPeopleDataResponse> {
        try {
            const [livesInHouse, maleOlder] = await Promise.all([
                this.peopleTable
                    .createQueryBuilder("people")
                    .where("user_id = :userId", { userId })
                    .getMany(),
                this.peopleTable.createQueryBuilder("people")
                    .where("user_id = :userId and sex='M' and age > 45", { userId })
                    .getMany()
            ]);

            const objectToReturn = {
                numberOfPeople: livesInHouse.length,
                numberOfOldMan: maleOlder.length
            };

            return objectToReturn;
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Returns like related data for specific user
     *
     * @param {number} userId
     * @returns
     * @memberof ReportManager
     */
    async getLikesData(userId: number): Promise<GetLikesDataResponse> {
        try {
            const [likesGiven, likesReceived] = await Promise.all([
                this.likesTable
                    .createQueryBuilder("likes")
                    .where("likes.a = :userId and likes.like = 1", { userId })
                    .getRawMany(),
                this.likesTable
                    .createQueryBuilder("likes")
                    .where("likes.b = :userId and likes.like = 1", { userId })
                    .getRawMany()
            ]);

            const matches: number[] = [];

            likesGiven.map(lg => {
                const lrr = likesReceived.find(lr => (lr.likes_a === lg.likes_b));
                if (lrr) {
                    matches.push(lrr.likes_a);
                }
            });

            const objectToReturn = {
                numberOfLikesGiven: likesGiven.length,
                listOfLikeGiven: likesGiven.map(l => l.likes_b).join(","),
                numberOfLikesReceived: likesReceived.length,
                numberOfMatches: matches.length,
                matches: matches.join(",")
            };

            return objectToReturn;
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Returns chat related data for specific user
     *
     * @param {number} userId
     * @returns
     * @memberof ReportManager
     */
    async geChatsData(userId: number): Promise<GetChatsDataResponse> {
        try {
            const [chats, sentChats, receivedChats] = await Promise.all([
                this.chatsTable
                    .createQueryBuilder("chats")
                    .select("chats.to")
                    .where("chats.from = :userId", { userId })
                    .distinct(true)
                    .getRawMany(),
                this.chatsTable
                    .createQueryBuilder("chats")
                    .where("chats.from = :userId", { userId })
                    .getRawMany(),
                this.chatsTable
                    .createQueryBuilder("chats")
                    .where("chats.to = :userId", { userId })
                    .getRawMany(),
            ]);

            const dif = sentChats.length - receivedChats.length;
            const objectToReturn = {
                numberOfDiffChats: chats.length,
                numberOfUnansweredChats: dif > 0 ? dif : 0
            };
            return objectToReturn;
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Maps property type
     *
     * @param {number} type
     * @returns
     * @memberof ReportManager
     */
    mapPropertyType(type: number): string {
        switch (type) {
            case 1:
                return "FLAT";
            case 0:
                return "-";
            case 2:
                return "small house";
            case 3:
                return "big house";
            case 4:
                return "Villa";
            default:
                return "-";
        }
    }

    /**
     * Generates pdf from JSON object
     *
     * @param {*} data
     * @returns
     * @memberof ReportManager
     */
    async generatePdf(data: any): Promise<{ done: boolean }> {
        try {
            const html = jsonObjectToHTML(data);
            await createPDF({
                html,
                outPath: `./reports-${data.userId}.pdf`
            });
            return { done: true };
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }
}
