import HTTP from "http-status-codes";
import { EntityManager, getConnection, getManager, Repository } from "typeorm";
import { Busstops, Postcodes } from "../entities";
import { GetAddressesLocationsResponse, GetBusLocationsResponse, GetLocationsResponse, GetSchoolLocationsResponse } from "../types/schema-generated";

import { codedError } from "../lib/coded-error";

export class LocationManager {

    private readonly postcodesTable: Repository<Postcodes>;
    private readonly busTable: Repository<Busstops>;
    private readonly entityManager: EntityManager;

    constructor() {
        this.postcodesTable = getConnection().getRepository(Postcodes);
        this.busTable = getConnection().getRepository(Busstops);
        this.entityManager = getManager();
    }

    /**
     * Get locations within specified filter
     *
     * @param {string} postcode
     * @param {string} filter
     * @returns {Promise<GetLocationsResponse>}
     * @memberof LocationManager
     */
    async get(postcode: string, filter: string): Promise<GetLocationsResponse> {
        switch (filter) {
            case "school":
                return this.tenMileSchools(postcode);
            case "bus":
                return this.nearestBusStops(postcode);
            case "address":
                return this.postcodeAddresses(postcode);
            default:
                throw codedError(HTTP.BAD_REQUEST, "There is no filter");
        }
    }

    /**
     * Returns all addresse on specific postcode
     *
     * @param {string} postcode
     * @returns {Promise<GetAddressesLocationsResponse>}
     * @memberof LocationManager
     */
    async postcodeAddresses(postcode: string): Promise<GetAddressesLocationsResponse> {
        try {
            const addresses = await getConnection()
                .getRepository(Postcodes)
                .createQueryBuilder("postcodes")
                .leftJoinAndSelect("postcodes.addresses", "addresses")
                .where(`postcode = "${postcode}"`)
                .getMany();

            if (!addresses[0]) {
                return { addresses: [] };
            }
            const mapAddresses = addresses[0].addresses.map(a => {
                return a;
            });
            return { addresses: mapAddresses };
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Returns 5 bus stops closest to postcode
     *
     * @param {string} postcode
     * @returns {Promise<GetBusLocationsResponse>}
     * @memberof LocationManager
     */
    async nearestBusStops(postcode: string): Promise<GetBusLocationsResponse> {
        try {
            const postcodeDetails = await this.postcodesTable
                .createQueryBuilder("postcodes")
                .where(`postcode = "${postcode}"`)
                .getOne();

            const selectionQuery = `id, name,
         ( ACOS( COS( RADIANS(  ${postcodeDetails!.latitude} ) )
                     * COS( RADIANS( lat ) )
                     * COS( RADIANS( lon ) - RADIANS( ${postcodeDetails?.longitude} ) )
                     + SIN( RADIANS(  ${postcodeDetails!.latitude}  ) )
                     * SIN( RADIANS( lat ) )
                 )
               * 6371) AS distance`;

            const busStopsToReturnData = await this.busTable
                .createQueryBuilder("")
                .select(selectionQuery)
                .orderBy("distance")
                .limit(5)
                .getRawMany();

            return {
                busStops: busStopsToReturnData.map(b => {
                    return { id: b.id, name: b.name };
                })
            };
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Returns all schools in a 10-mile radius
     *
     * @param {string} postcode
     * @param {string} filter
     * @returns {Promise<GetSchoolLocationsResponse>}
     * @memberof LocationManager
     */
    async tenMileSchools(postcode: string): Promise<GetSchoolLocationsResponse> {
        try {
            const postcodeDetails = await this.postcodesTable
                .createQueryBuilder("postcodes")
                .where(`postcode = "${postcode}"`)
                .getOne();

            const schoolsToReturn = await this.entityManager.query(`
            SELECT * FROM
            (SELECT id, postcode,
                    ( ACOS( COS( RADIANS( ${postcodeDetails?.latitude}  ) )
                                * COS( RADIANS( latitude ) )
                                * COS( RADIANS( longitude ) - RADIANS( ${postcodeDetails?.longitude} ) )
                                + SIN( RADIANS(  ${postcodeDetails?.latitude}  ) )
                                * SIN( RADIANS( latitude ) )
                            )
                          * 6371) AS distance FROM postcodes  HAVING distance < 10
           ) p
           INNER JOIN schools ON schools.postcode_id = p.id
           `
            ) as { id: string, postcode: string, distance: number, name: string, postcode_id: string, }[];

            return {
                schools: schoolsToReturn.map(s => {
                    return { id: s.id, name: s.name };
                })
            };
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }

    /**
     * Calculates distance between two points
     *
     * @param {number} lat1
     * @param {number} lon1
     * @param {number} lat2
     * @param {number} lon2
     * @returns {number}
     * @memberof LocationManager
     */
    distance(lat1: number, lon1: number, lat2: number, lon2: number): number {
        const R = 3963;
        const fi1 = lat1 * Math.PI / 180;
        const f2 = lat2 * Math.PI / 180;
        const deltaF = (lat2 - lat1) * Math.PI / 180;
        const deltaL = (lon2 - lon1) * Math.PI / 180;

        const a = Math.sin(deltaF / 2) * Math.sin(deltaF / 2) +
            Math.cos(fi1) * Math.cos(f2) *
            Math.sin(deltaL / 2) * Math.sin(deltaL / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return c * R;
    }
}
