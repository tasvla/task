import HTTP from "http-status-codes";
import { getConnection, Repository } from "typeorm";
import { Postcodes } from "../entities";
import { codedError } from "../lib/coded-error";
import { GetPostcodesResponse } from "../types";

export class PostcodeManager {

    private readonly postcodesTable: Repository<Postcodes>;

    constructor() {
        this.postcodesTable = getConnection().getRepository(Postcodes);
    }

    /**
     * Returns all postcodes grouped by the start of the postcode
     *
     * @returns {Promise<GetPostcodesResponse>}
     * @memberof PostcodeManager
     */
    async getAll(): Promise<GetPostcodesResponse> {
        try {
            const postcodes = await this.postcodesTable
                .createQueryBuilder("postcodes")
                .getMany();
            return postcodes.reduce((acc, param) => {
                if (param.postcode) {
                    if (acc[param.postcode.split(" ")[0]]) {
                        acc[param.postcode.split(" ")[0]] = [
                            ...acc[param.postcode.split(" ")[0]],
                            param.postcode
                        ];
                    } else {
                        acc[param.postcode.split(" ")[0]] = [
                            param.postcode
                        ];
                    }

                }
                return acc;
            }, {} as any);
        } catch (error) {
            throw codedError(HTTP.INTERNAL_SERVER_ERROR, error.message);
        }
    }
}
