export interface GetPostcodesResponse {
    [k: string]: string[];
}
