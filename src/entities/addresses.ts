import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Houses } from "./houses";

import { Postcodes } from "./postcodes";

// tslint:disable
@Entity()
export class Addresses {

    @PrimaryGeneratedColumn(
        "increment",
        {
            type: "int",
            unsigned: true,
        }
    )
    id: number;

    @JoinColumn({
        name: "postcode_id"
    })
    @ManyToOne(() => Postcodes,
        postcode => postcode.addresses
    )
    postcode: Postcodes

    @OneToMany(() => Houses,
        houses => houses.postcode)
    houses: Houses[]

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    district: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    locality: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    street: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    site: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 20
    })
    site_number: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    site_description: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    site_subdescription: string;
}
