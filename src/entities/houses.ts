import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Addresses } from "./addresses";

import { Users } from "./index";
import { Postcodes } from "./postcodes";

// tslint:disable
@Entity()
export class Houses {

    @PrimaryGeneratedColumn(
        "increment",
        {
            type: "int",
            unsigned: true,
        }
    )
    id: number;

    @JoinColumn({ name: "user_id" })
    @OneToOne(type => Users,
        { nullable: false }
    )
    user: Users;

    @JoinColumn({ name: "postcode_id" })
    @ManyToOne(type => Postcodes,
        postcode => postcode.houses
    )
    postcode: Postcodes;


    @JoinColumn({ name: "address_id" })
    @ManyToOne(type => Addresses,
        address => address.houses,
        { nullable: false }
    )
    address: Addresses

    @Column({
        type: "tinyint",
        nullable: false,
        unsigned: true,
        width: 4
    })
    propertytype: number;

    @Column({
        nullable: false,
        type: "date"
    })
    updated: Date;
}
