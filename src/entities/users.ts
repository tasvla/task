// tslint:disable:max-classes-per-file
import {
    Column,
    Entity,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Houses } from "./houses";
import { People } from "./people";
import { Photos } from "./photos";

// tslint:disable
@Entity()
export class Users {
    @PrimaryGeneratedColumn({
        unsigned: true
    })
    id: number;

    @Column({
        nullable: false,
        type: "varchar",
        length: 80
    })
    name: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 80
    })
    surname: string;

    @Column({
        nullable: false,
        type: "varchar",
        length: 100
    })
    email: string;

    @OneToMany(type => Photos, photo => photo.user)
    photos: Photos[];

    @OneToMany(type => People, people => people.user)
    people: People[];

    @OneToOne(() => Houses, house => house.user)
    house: Houses
}
