import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from "typeorm";

// tslint:disable
@Entity()
export class Busstops {

    @PrimaryGeneratedColumn(
        "increment",
        {
            type: "int",
            unsigned: true,
        }
    )
    id: number;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    name: string;

    @Column({
        nullable: false,
        type: "decimal",
        precision: 11,
        scale: 8
    })
    lat: number;

    @Column({
        nullable: false,
        type: "decimal",
        precision: 11,
        scale: 8
    })
    lon: number;
}
