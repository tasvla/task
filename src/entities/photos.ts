import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Users } from "./users";

// tslint:disable
@Entity()
export class Photos {

    @PrimaryGeneratedColumn({ unsigned: true })
    id: number;

    @Column({
        nullable: false,
        type: "varchar",
        length: 60
    })
    path: string;

    @Column({
        nullable: false,
        type: "date"
    })
    updated: number;

    @ManyToOne(type => Users, { nullable: false })
    @JoinColumn({
        name: 'user_id'
    })
    user: Users;
}
