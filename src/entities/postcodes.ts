import {
    Column,
    Entity,
    OneToMany,
    PrimaryColumn,
} from "typeorm";
import { Addresses } from "./addresses";
import { Houses } from "./houses";
import { Schools } from "./schools";

// tslint:disable
@Entity()
export class Postcodes {

    @PrimaryColumn({
        unsigned: true,
        nullable: false,
        width: 11
    })
    id: number;

    @Column({
        nullable: true,
        type: "nchar",
        width: 10
    })
    postcode: string;

    @Column({
        nullable: true,
        type: "float"
    })
    latitude: number;

    @Column({
        nullable: true,
        type: "float"
    })
    longitude: number;

    @OneToMany(() => Addresses,
        addresses => addresses.postcode)
    addresses: Addresses[]

    @OneToMany(() => Houses,
        houses => houses.postcode)
    houses: Houses[]

    @OneToMany(() => Schools,
        school => school.postcode)
    schools: Schools[]
}
