import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from "typeorm";

// tslint:disable
@Entity()
export class Likes {

    @PrimaryGeneratedColumn(
        "increment",
        {
            type: "int",
            unsigned: true,
        }
    )
    id: number;

    @Column({
        nullable: false,
        type: "int",
        unsigned: true,
        width: 11
    })
    a: number;

    @Column({
        nullable: false,
        type: "int",
        unsigned: true,
        width: 11
    })
    b: number;

    @Column({
        nullable: false,
        type: "tinyint",
        unsigned: true,
        width: 3
    })
    like: number;

    @Column({
        nullable: false,
        type: "datetime",
    })
    date: Date;
}
