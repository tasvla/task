import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from "typeorm";

import { Users } from "./index";

// tslint:disable
@Entity()
export class People {

    @PrimaryGeneratedColumn(
        "increment",
        {
            type: "int",
            unsigned: true,
        }
    )
    id: number;

    @Column({
        nullable: false,
        type: "int"
    })
    age: number;

    @Column({
        nullable: false,
        type: "nchar",
        width: 3
    })
    sex: string;

    @JoinColumn({
        name: "user_id",
    })
    @ManyToOne(type => Users,
        user => user.people,
        { nullable: false })
    user: Users;

}
