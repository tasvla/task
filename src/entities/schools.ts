import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Postcodes } from "./postcodes";

// tslint:disable
@Entity()
export class Schools {

    @PrimaryGeneratedColumn({
        unsigned: true,
    })
    id: number;

    @Column({
        nullable: false,
        type: "varchar",
        length: 100
    })
    name: string;

    @JoinColumn({
        name: "postcode_id"
    })
    @ManyToOne(type => Postcodes,
        postcode => postcode.schools,
        { nullable: false }
    )
    postcode: Postcodes;
}