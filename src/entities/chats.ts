import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
} from "typeorm";

// tslint:disable
@Entity()
export class Chats {

    @PrimaryGeneratedColumn(
        "increment",
        {
            type: "int",
            unsigned: true,
        }
    )
    id: number;

    @Column({
        nullable: false,
        type: "int",
    })
    from: number;

    @Column({
        nullable: false,
        type: "int",
    })
    to: number;

    @Column({
        nullable: false,
        type: "text",
        charset: "utf8"
    })
    message: string;

    @Column({
        nullable: false,
        type: "datetime"
    })
    date: Date;

    @Column({
        nullable: false,
        type: "datetime"
    })
    seendate: Date;
}
